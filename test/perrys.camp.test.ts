import { expect, test, beforeEach } from 'vitest'
import {
    deleteMessageById,
    updateMessageById,
    getMessageById,
    createUser,
    createMessage,
    listMessagesBetweenUsers,
    getAllUsers
} from '../utils/apiHelper'

let user1, user2

beforeEach(async () => {
    user1 = await createUser(`user-${Date.now()}`)
    user2 = await createUser(`user-${Date.now()}`)
})

test('Post a message', async () => {
    await createMessage(user1.name, user2.name, 'hi')
})

test('Get all messages between 2 users', async () => {
    // User 1 greets user 2
    await createMessage(user1.name, user2.name, 'hi')
    let messages = await listMessagesBetweenUsers(user1.name, user2.name)
    expect(messages.length).toBe(1)
    expect(messages[0].from.id).toBe(user1.name)
    expect(messages[0].to.id).toBe(user2.name)
    await createMessage(user1.name, user2.name, 'are you there')
    messages = await listMessagesBetweenUsers(user1.name, user2.name)
    expect(messages.length).toBe(2)
    expect(messages[1].from.id).toBe(user1.name)
    expect(messages[1].to.id).toBe(user2.name)

    // User 2 responds
    messages = await listMessagesBetweenUsers(user2.name, user1.name)
    expect(messages.length).toBe(0)
    await createMessage(user2.name, user1.name, 'yeah whats up')
    messages = await listMessagesBetweenUsers(user2.name, user1.name)
    expect(messages.length).toBe(1)
})

test('Get a message by id', async () => {
    const message = await createMessage(user1.name, user2.name, 'hi')
    const messageInfo = await getMessageById(message.id)
    expect(messageInfo.from.id).toBe(user1.name)
    expect(messageInfo.to.id).toBe(user2.name)
})

test('Delete a messsage by id', async () => {
    const message = await createMessage(user1.name, user2.name, 'hi')
    await deleteMessageById(message.id)
    try {
        await getMessageById(message.id)
        throw new Error('Was able to find deleted message')
    } catch (error) {
        expect(error.expected).toBe(200)
        expect(error.actual).toBe(404)
    }
})

/**
 * FAILING - Endpoint tries to typecast newMessage as a User for some reason?
 */
test('Edit a message by id', async () => {
    const beforeMessage = 'hows it goign'
    const afterMessage = 'hows it going'  // User fixed the typo in "going"
    const message = await createMessage(user1.name, user2.name, beforeMessage)
    const messageInfoBefore = await getMessageById(message.id)
    expect(messageInfoBefore.message).toBe(beforeMessage)
    await updateMessageById(message.id, afterMessage)
    const messageInfoAfter = await getMessageById(message.id)
    expect(messageInfoAfter.message).toBe(afterMessage)
})

test('Get all users', async () => {
    const usersBefore = await getAllUsers()
    expect(usersBefore.length).toBeGreaterThanOrEqual(0)
    const newUser = await createUser(`user-${Date.now()}`)
    const usersAfter = await getAllUsers()
    expect(usersAfter.length).toBeGreaterThanOrEqual(usersBefore.length)
    expect(usersAfter.find(user => user.name = newUser.name)).toBeTruthy()
})