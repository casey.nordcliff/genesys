Hi there! In this repository, you will find a standard node
project set up to test Perry's Summer Camp. To run the tests,
simply:

0) Have Perrys Summer Camp already running (tests will use the same ports as the service)
1) Install dependencies via `npm ci`
2) Run the tests via `npm test`

Thats it! I've handled the rest, and the results will be
printed out to your terminal.


(Optional run down of the tech stack, feel free to skip this)
1) Vitest: the testing framework from the same people who made Vite,
   the frontend transpiler/bundler
2) node-fetch, a small 1-to-1 copy of the 'fetch' API available for browsers


What was tested:
Basic endpoint functionality for all Message API endpoints as described by instructions
and the Postman collection. Inside of these were a few functional tests ensuring the service
works as per the listed requirements

Bugs found:

1) While its not explicitly stated, you need Node v14 to install packages. This is an old
   version that is out of date. I had to trial-and-error my way to a working Node version.
   
   SOLUTION: Explicitly state what version you need, or put an "engine" field in package.json

2) It was hard to figure out exactly what form various APIs needed. The docs said to just do a
   POST on /users, but you needed to do more than a simple post.

   SOLUTION: In general, just update the docs and the instructions to be informative.

3) Creating a new entity should return a 201, not a 200. In general, http status codes
   were not what they should have been.

   SOLUTION: Return the appropriate status types for things.

4) API Docs vs written instruction was not always in agreement; updating a message,
   for example. The instructions say the endpoint is /message, but the
   Postman collection says its /messages, plural.

5) The update message endpoint appears to be broken. It should take a message id and
   a new message and update it. Instead, and tries to read the new message off the body,
   but then it tries to typecast that as a User type, which doesn't make sense