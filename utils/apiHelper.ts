import fetch from 'node-fetch'
import { expect } from 'vitest'

const port = process.env.PORT || 3000
const baseURL = `http://localhost:${port}/api`

export async function createUser(name: string) {
    const res = await fetch(`${baseURL}/users`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
        body: new URLSearchParams({
            'name': `${name}`,
        })
    })
    expect(res.status).toBe(200)
    return res.json()
}

export async function createMessage(fromUserId: string, toUserId: string, message: string) {
    const res =  await fetch(`${baseURL}/messages`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify({
            from: { id: fromUserId },
            to: { id: toUserId },
            message
        })
    })
    expect(res.status).toBe(200)
    return res.json()
}

export async function listMessagesBetweenUsers(fromUserId: string, toUserId: string) {
    const res =  await fetch(`${baseURL}/messages?from=${fromUserId}&to=${toUserId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
    expect(res.status).toBe(200)
    const data = await res.json()
    return data
}

export async function getMessageById(id: string) {
    const res =  await fetch(`${baseURL}/messages/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
    expect(res.status).toBe(200)
    const data = await res.json()
    return data
}

export async function deleteMessageById(id: string) {
    const res =  await fetch(`${baseURL}/messages/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
    expect(res.status).toBe(204)
    return res
}

export async function updateMessageById(id: string, newMessage: string) {
    const res =  await fetch(`${baseURL}/messages/${id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        },
        body: JSON.stringify({
            message: newMessage
        })
    })
    expect(res.status).toBe(200)
    return res.json()
}

export async function getAllUsers() {
    const res =  await fetch(`${baseURL}/users`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json; charset=UTF-8'
        }
    })
    expect(res.status).toBe(200)
    return res.json()
}